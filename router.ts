// import express, { Request, Response } from "express";
// import { User } from "./src/models/user";
// import * as bcrypt from "bcrypt";
// import { LoginRoute } from "./src/routes/login";
// import userRoute from "./src/routes/user";

// const router = express.Router();

// // router.get("/", (req: Request, res: Response) => {
// //   var data = new User({
// //     firstName: req.body.firstName,
// //     lastName: req.body.lastName,
// //     email: req.body.email,
// //     mobileNumber: req.body.mobileNumber,
// //     password: req.body.password,
// //   });
// //   console.log("data", data);

// //   res.send("Router");
// // });

// router.post("/", async (req, res) => {
//   const user = req.body;

//   //check user already exists
//   const takenUserName = await User.findOne({ username: user.username });
//   const takenEmail = await User.findOne({
//     email: user.email,
//   });

//   if (takenUserName) {
//     return res.status(500).json({
//       message: "User already Exists with this name.",
//     });
//   }
//   if (takenEmail) {
//     return res.status(500).json({
//       message: "User already Exists with this email address.",
//     });
//   }

//   let newUser = new User(req.body);

//   newUser.password = await bcrypt.hash(req.body.password, 10);
//   newUser.save((err: any) => {
//     if (err) {
//       return res.status(400).json({
//         error: err,
//       });
//     }
//     return res.status(200).json({
//       message: "User saved successfully.",
//     });
//   });
// });

// module.exports = router;

// router.get("/", async (req: Request, res: Response) => {
//   // await User.create({
//   //   firstName: req.body.firstName,
//   //   lastName: req.body.lastName,
//   //   email: req.body.email,
//   //   mobileNumber: req.body.mobileNumber,
//   //   password: req.body.password,
//   // });
//   var data = new User({
//     firstName: req.body.firstName,
//     lastName: req.body.lastName,
//     email: req.body.email,
//     mobileNumber: req.body.mobileNumber,
//     password: req.body.password,
//   });
//   console.log("data", data);

//   res.send("Status Ok");
// });

// router.use(userRoute);
// router.use(LoginRoute);
