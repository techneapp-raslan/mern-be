// Third Party Modules
import express, { Express, Request, Response } from "express";
const app: Express = express();
import bodyParser from "body-parser";
import cors from "cors";
import mongoose from "mongoose";
const InfoRouter = require("./router");
const router = express.Router();
const { UserSchema } = require("./src/models/user");
import { User } from "./src/models/user";

import LoginRoute from "./src/routes/login";
import userRoute from "./src/routes/user";
import dotenv from "dotenv";

const InfoRouter2 = require("./router");

dotenv.config();

// const origin = ["http://localhost:3001/"];

// const options: cors.CorsOptions = { origin: origin };

app.use(cors());
// Bodyparser middleware
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());
app.use(express.json());

// app.use("/", (req: Request, res: Response) => {
//   res.send("Server Is Working");
// });

// App Listen Port
app.listen(process.env.PORT, function () {
  console.log("Server is running on Port: " + process.env.PORT);
});

// Db Connection
mongoose.connect("mongodb://localhost:27017/mern", (err: any) => {
  if (!err) {
    console.log("Connection established");
  }
});

//Router
app.use(userRoute);
app.use(LoginRoute);
