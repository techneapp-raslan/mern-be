// import mongoose from "mongoose";

// const UserSchema = new mongoose.Schema(
//   {
//     firstName: {
//       type: String,
//       required: true,
//     },
//     lastName: {
//       type: String,
//       required: true,
//     },
//     email: {
//       type: String,
//       required: true,
//       unique: true,
//     },
//     mobileNumber: {
//       type: Number,
//       required: true,
//     },
//     password: {
//       type: String,
//       required: true,
//     },
//   },
//   { collection: "user_data" }
// );

// module.exports = { UserSchema };

import { model, Schema, Model, Document } from "mongoose";

export interface IUser extends Document {
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phoneNumber: string;
  password: string;
  createDate: Date;
  updatedDate: Date;
}

const UserSchema: Schema = new Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  username: { type: String, required: true },
  email: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  password: { type: String, required: true },
  createDate: { type: Date, default: Date.now },
  updatedDate: { type: Date, default: Date.now },
});

export const User: Model<IUser> = model<IUser>("Users", UserSchema);
