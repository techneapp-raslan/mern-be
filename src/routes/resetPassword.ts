import express, { Router, Request, Response } from "express";

import * as bcrypt from "bcrypt";
import { User } from "../models/user";
import { verifyJWT } from "../middleware/auth";

const ResetPasswordRoute = express.Router();

ResetPasswordRoute.post("/reset-password", verifyJWT, async (req, res) => {
  const Userdetails = req.body;
  const user = await User.findOne({ email: Userdetails.email });

  if (!user) {
    return res.status(500).json({
      message: "Email address not found.",
    });
  } else {
    const newPassword = await bcrypt.hash(req.body.password, 10);
    User.findByIdAndUpdate(
      user.id,
      {
        $set: { password: newPassword },
      },
      (err: any, user: any) => {
        if (err) {
          console.log(err);
          return res.status(400).json({
            error: err.message,
          });
        }
        return res.status(200).json({
          message:
            "Your Password Has Been Successfully Reset , Please Return to the SignIn Page to SignIn ",
          user: user,
        });
      }
    );
  }
});

export default ResetPasswordRoute;
