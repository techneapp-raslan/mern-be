import express, { Router, Request, Response } from "express";

import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";
import { User } from "../models/user";

const LoginRoute = express.Router();

LoginRoute.post("/login", async (req: Request, res: Response) => {
  const login = req.body;

  User.findOne({ username: login.username }).then((user) => {
    if (!user) {
      return res.status(500).json({
        message: "Invalid username or password.",
      });
    }
    bcrypt.compare(login.password, user.password).then((isValidPassword) => {
      if (isValidPassword) {
        const paylod = {
          id: user.id,
          username: user.username,
        };

        jwt.sign(
          paylod,
          process.env.JWT_SCRETE_KEY as string,
          { expiresIn: 86400 },
          (err, token) => {
            if (err) {
              return res.status(400).json({
                message: err.message,
              });
            }
            return res.status(200).json({
              message: "Login successfully.",
              token: token,
            });
          }
        );
      } else {
        return res.status(500).json({
          message: "Invalid username or password.",
        });
      }
    });
  });
});

export default LoginRoute;
