import express, { NextFunction, Request, Response } from "express";

import * as bcrypt from "bcrypt";
import { User } from "../models/user";
import { verifyJWT } from "../middleware/auth";

const userRoute = express.Router();

userRoute.post("/api/user/register", async (req: Request, res: Response) => {
  debugger;
  const user = req.body;

  //check user already exists
  const takenUserName = await User.findOne({ username: user.username });
  const takenEmail = await User.findOne({
    email: user.email,
  });

  if (takenUserName) {
    return res.status(500).json({
      message: "User already Exists with this name.",
    });
  }
  if (takenEmail) {
    return res.status(500).json({
      message: "User already Exists with this email address.",
    });
  }

  let newUser = new User(req.body);

  newUser.password = await bcrypt.hash(req.body.password, 10);
  newUser.save((err: any) => {
    if (err) {
      return res.status(400).json({
        error: err,
      });
    }
    return res.status(200).json({
      message: "User saved successfully.",
    });
  });
});

//express http req
userRoute.get("/api/user", verifyJWT, (req, res) => {
  //momgodb query function
  User.find().exec((err, data) => {
    if (err) {
      return res.status(400).json({
        error: err,
      });
    }
    return res.status(200).json({
      data: data,
    });
  });
});

userRoute.put("/api/user/edit/:id", verifyJWT, (req, res) => {
  User.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body,
    },
    (err: any, user: any) => {
      if (err) {
        return res.status(400).json({
          error: err,
        });
      }
      return res.status(200).json({
        message: "User saved successfully.",
        user: user,
      });
    }
  );
});

userRoute.delete("/api/user/delete/:id", verifyJWT, (req, res) => {
  User.findByIdAndDelete(req.params.id).exec((err: any, user: any) => {
    if (err) {
      return res.status(400).json({
        error: err,
      });
    }
    return res.status(200).json({
      message: "User deleted successfully.",
      user: user,
    });
  });
});

export default userRoute;
