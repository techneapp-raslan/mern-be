import { NextFunction, Request, Response } from "express";
import { User } from "../models/user";
import * as jwt from "jsonwebtoken";

function verifyJWT(req: Request, res: Response, next: NextFunction) {
  const token = req.headers["x-access-token"];

  if (token) {
    jwt.verify(token as string, process.env.JWT_SCRETE_KEY as string, (err) => {
      if (err)
        return res.json({
          isLoggedIn: false,
          message: "Failed To Authenticate",
        });

      next();
    });
  } else {
    res.json({ isLoggedIn: false, message: "Invalid Token" });
  }
}

export { verifyJWT };
